# execute 'apt-get update'
exec { 'apt-update':                    # exec resource named 'apt-update'
  command => '/usr/bin/apt-get update'  # command this resource will run
}

# install apache2 package
package { 'apache2':
  require => Exec['apt-update'],        # require 'apt-update' before installing
  ensure => installed,
} ->
service { 'apache2':
  ensure => running,
} -> 
exec { 'remove_html_folder':
  command => "/bin/rm -Rf /var/www/html"
} ->
exec { 'link_to_app':
  command => "/bin/ln /home/vagrant/app/current /var/www/html -s"
}

# install php-7.0 package
package { 'php7.0':
  require => Exec['apt-update'],        # require 'apt-update' before installing
  ensure => installed,
}

# install php7.0-mysql package
package { 'php7.0-mysql':
  require => Exec['apt-update'],        # require 'apt-update' before installing
  ensure => installed,
  notify  => Service['apache2']
}

# install libapache2-mod-php7.0 package
package { 'libapache2-mod-php7.0':
  require => Exec['apt-update'],        # require 'apt-update' before installing
  ensure => installed,
}

file { '/home/vagrant/puppet/modules/install_mysql.sh':
  ensure => 'file',
  path => '/home/vagrant/puppet/modules/install_mysql.sh',
  owner => 'root',
  group => 'root',
  mode  => '0755', 
  notify => Exec['install_mysql_script'],
}

exec { 'install_mysql_script':
  command => "/bin/bash -c '/home/vagrant/puppet/modules/install_mysql.sh'",
}


